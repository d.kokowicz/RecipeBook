//
//  FirstScreenCoordinatorDelegate.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 29/06/2021.
//

import Foundation

protocol FirstScreenCoordinatorDelegate: NSObject {
  func startSecondScreenFlow()
}
