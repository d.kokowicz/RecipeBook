//
//  UIWindow.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 24/06/2021.
//

import UIKit

fileprivate enum WindowAnimation {
  static let rootTransitionDuration = 0.3
  static let scaleFactor: CGFloat = 1.5
}

extension UIWindow {
  func setRootController(_ controller: UIViewController, animated: Bool = false) {
    guard let currentRootController = rootViewController,
          let snapshotView = currentRootController.view.snapshotView(afterScreenUpdates: false), animated else {
      rootViewController = controller
      return
    }
    
    controller.view.addSubview(snapshotView)
    
    rootViewController = controller
           UIView.animate(withDuration: WindowAnimation.rootTransitionDuration, animations: {
               snapshotView.layer.opacity = 0
               snapshotView.layer.transform = CATransform3DMakeScale(WindowAnimation.scaleFactor, WindowAnimation.scaleFactor, WindowAnimation.scaleFactor)
           }, completion: { (finished) -> Void in
               snapshotView.removeFromSuperview()
           })
       }
   }
