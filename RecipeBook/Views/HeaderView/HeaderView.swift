//
//  HeaderView.swift
//  TestAppRecipe
//
//  Created by Dominika Kokowicz on 02/07/2021.
//

import UIKit
import Reusable
import AlamofireImage

class HeaderView: UIImageView, NibReusable {
  
  @IBOutlet weak var recipeTitle: UITextView!
  @IBOutlet weak var recipeImage: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    recipeTitle.isScrollEnabled = false
    recipeTitle.sizeToFit()
  }
  
  func configure(with viewModel: HeaderViewModel?) {
    guard let viewModel = viewModel else { return }
    recipeTitle.text = viewModel.title
    guard let imageURL = URL(string: viewModel.foodImageURL) else { return }
    recipeImage.af.setImage(withURL: imageURL)
  }
  
  
}
