//
//  SelectRecipeTableViewCell.swift
//  TestAppRecipe
//
//  Created by Dominika Kokowicz on 05/07/2021.
//

import UIKit
import Reusable
import AlamofireImage
import SVProgressHUD

class SelectRecipeTableViewCell: UITableViewCell, NibReusable  {
  
  @IBOutlet weak var foodImage: UIImageView!
  @IBOutlet weak var titleTextField: UITextView!
  @IBOutlet weak var readyAndServingsLabel: UILabel!
  @IBOutlet weak var descriptionTextField: UITextView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.selectionStyle = .none
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  func configure(with viewModel: SelectRecipeTVCellModel?) {
    SVProgressHUD.show()
    guard let viewModel = viewModel else { return }
    titleTextField.text = viewModel.titleTextField
    readyAndServingsLabel.text = viewModel.readyAndServingsLabel
    
    do {
      if let data = viewModel.descriptionTextField.data(using: .unicode) {
        let attributed = try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        descriptionTextField.attributedText = attributed
      }
    } catch {
      print("Error during convertion of attributed text: \(error)")
      descriptionTextField.text = viewModel.descriptionTextField
    }
    
    guard let imageURL = URL(string: viewModel.foodImageURL) else { return }
    foodImage.af.setImage(withURL: imageURL)
    SVProgressHUD.dismiss()
  }
  
}


