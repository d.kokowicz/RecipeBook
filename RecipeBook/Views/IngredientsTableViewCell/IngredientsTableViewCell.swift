//
//  IngredientsTableViewCell.swift
//  TestAppRecipe
//
//  Created by Dominika Kokowicz on 30/06/2021.
//

import UIKit
import Reusable

class IngredientsTableViewCell: UITableViewCell, NibReusable {
  
  @IBOutlet weak var ingredientName: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.selectionStyle = .none
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    self.accessoryType = selected ? .checkmark : .none
  }
  
  func configure(with viewModel: IngredientsTVCellModel?) {
    ingredientName.text = viewModel?.ingredientName
    ingredientName.sizeToFit()
    ingredientName.lineBreakMode = .byWordWrapping
    ingredientName.numberOfLines = 0
    self.accessoryView?.backgroundColor = .blue
  }
  
}

