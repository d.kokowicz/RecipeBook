//
//  RecipeTableViewCellViewData.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 15/07/2021.
//

import Foundation
import UserNotifications

class RecipeTableViewCellViewModel: NSObject {
  
  weak var managedCell: RecipeTableViewCell?
  var viewData: RecipeTVCellModel?
  var shouldTimerBeHidden = false
  var showAlertIsEnabled = false
  var timerStartDate: Date?
  var timerStopDate: Date?
  var appDidBecomeActiveDate: Date?
  var timer: Timer?
  var timeRemaining: Int = 0
  var initialTimeCountdown: Int = 0
  let center = UNUserNotificationCenter.current()
  
  override init() {
    super.init()
    center.delegate = self
  }
  
  struct AlertAndNotification {
    static let title = "You have finished a cooking step"
    static let message = "Let's move to another step and continue cooking 🍽!"
  }
  
//MARK: Alert
  func showAlert() {
    DispatchQueue.main.async { [self] in
      managedCell?.presentAlert(title: AlertAndNotification.title,
                                message: AlertAndNotification.message)
    }
  }
}

//MARK: Notifications
extension RecipeTableViewCellViewModel: UNUserNotificationCenterDelegate {
  func checkAuthorization() {
    center.getNotificationSettings { settings in
      switch settings.authorizationStatus {
      case .authorized:
        self.createNotification()
      case .denied:
        self.showAlertIsEnabled = true
      case .provisional, .ephemeral:
        self.createNotification()
        self.showAlertIsEnabled = true
      case .notDetermined:
        self.requestAuthorization()
      @unknown default:
        self.showAlertIsEnabled = true
      }
    }
  }
  
  func requestAuthorization() {
    center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
      if granted {
        print("Request authorization was granted")
      } else if !granted {
        print("Request authorization not granted")
      } else {
        print("Error when requesting authorization:\(String(describing: error?.localizedDescription))")
      }
    }
  }
  
  func createNotification() {
    guard (initialTimeCountdown > 0) else { return }
    let content = UNMutableNotificationContent()
    content.title = AlertAndNotification.title
    content.body = AlertAndNotification.message
    content.sound = .default
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(timeRemaining), repeats: false)
    let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
    
    center.add(request) { (error : Error?) in
      if let theError = error {
        print("Error when adding notification to the center: \(theError.localizedDescription)")
      }
    }
    
  }
  
  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    print("Notification is about to be presented")
    completionHandler([.badge, .sound, .alert])
  }
  
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    let identifier = response.actionIdentifier
    
    switch identifier {
    case UNNotificationDismissActionIdentifier:
      print("The notification was dismissed")
      completionHandler()
    case UNNotificationDefaultActionIdentifier:
      print("The user opened app from the notification")
      completionHandler()
    default:
      print("The default case was called")
      completionHandler()
    }
  }
  
}

//MARK: Timer
extension RecipeTableViewCellViewModel {
  @objc func fireTimer(with time: Int) {
    managedCell?.updateTimerLabel()
    
    if timeRemaining > 0 {
      timeRemaining -= 1
    } else {
      if showAlertIsEnabled == true {
        showAlert()
      }
      timer?.invalidate()
      timer = nil
      
      timeRemaining = initialTimeCountdown
    }
    managedCell?.updateTimerLabel()
  }
  
  func calculateTimePassed() {
    if let appDidBecomeActiveDate = appDidBecomeActiveDate, let timerStopDate = timerStopDate {
      print("Time passed when app was in background: \(appDidBecomeActiveDate.timeIntervalSince(timerStopDate))")
    }
    if let appDidBecomeActiveDate = appDidBecomeActiveDate, let timerStartDate = timerStartDate {
      let passedTime = appDidBecomeActiveDate.timeIntervalSince(timerStartDate)
      timeRemaining = initialTimeCountdown - Int(passedTime)
    }
    appDidBecomeActiveDate = nil
    timerStartDate = nil
    timerStopDate = nil
    restartTimerAfterAppDidBecomeActive()
  }
//  func hideTimerIfTimeZero() {
//    managedCell?.hideTimerIfTimeZero()
//  }
//  
  func restartTimerAfterAppDidBecomeActive() {
    if timeRemaining < 0 {
      timeRemaining = 0
    } else {
      timerButtonTapped()
    }
  }
  
  func formatTimerString() -> String {
    let min = timeRemaining / 60
    let sec = timeRemaining % 60
    let timerString = String.init(format: "%02d : %02d", min, sec)
    return timerString
  }
  
  func stopTimer() {
    timerStopDate = Date()
    timer?.invalidate()
    timer = nil
    
    timeRemaining = initialTimeCountdown
    managedCell?.updateTimerLabel()
  }
  
  func timerButtonTapped() {
    checkAuthorization()
    timerStartDate = Date()
    
    guard (initialTimeCountdown > 0) else { return }
    
    if timer == nil {
      timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
      RunLoop.current.add(timer!, forMode: .common)
      timer?.tolerance = 0.1
    }
  }
  
}

