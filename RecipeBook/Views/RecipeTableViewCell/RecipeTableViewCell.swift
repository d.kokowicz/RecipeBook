//
//  RecipeTableViewCell.swift
//  TestAppRecipe
//
//  Created by Dominika Kokowicz on 29/06/2021.
//

import UIKit
import Reusable
import UserNotifications

final class RecipeTableViewCell: UITableViewCell, NibReusable {
  
  private var viewDataManager = RecipeTableViewCellViewModel()
  
  @IBOutlet weak var numberLabel: UILabel!
  @IBOutlet weak var timerLabel: UILabel!
  @IBOutlet weak var textFieldRecipeStep: UITextView!
  @IBOutlet weak var timerButton: UIButton!
  @IBOutlet weak var stopButton: UIButton!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.selectionStyle = .none
    viewDataManager.managedCell = self
    configure(with: viewDataManager.viewData)
    //hideTimerIfTimeZero()
    addObservers()
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  func configure(with viewModel: RecipeTVCellModel?) {
    guard let viewModel = viewModel else { return }
    numberLabel.text = String(viewModel.stepNumber)
    numberLabel.sizeToFit()
    timerLabel.sizeToFit()
    timerLabel.adjustsFontSizeToFitWidth = true
    textFieldRecipeStep.text = viewModel.stepDescription
    viewDataManager.timeRemaining = viewModel.stepDurationTime * 60
    viewDataManager.initialTimeCountdown = viewModel.stepDurationTime * 60
    if viewModel.stepDurationTime == 0 {
      //viewModelManager.shouldTimerBeHidden = true
      //hideTimerIfTimeZero()
    }
    textFieldRecipeStep.centerContentVertically()
    updateTimerLabel()
  }
  
//  func hideTimerIfTimeZero() {
//    if timerButton != nil, timerLabel != nil, stopButton != nil && viewModelManager.shouldTimerBeHidden == true {
//      timerButton.removeFromSuperview()
//      timerLabel.removeFromSuperview()
//      stopButton.removeFromSuperview()
//    }
//  }
  
}

//MARK: Observers
extension RecipeTableViewCell {
  func addObservers() {
    NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackgroundObserver), name: UIApplication.didEnterBackgroundNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActiveObserver), name: UIApplication.didBecomeActiveNotification, object: nil)
  }
  
  @objc func didEnterBackgroundObserver(notification: NSNotification) {
    viewDataManager.stopTimer()
  }
  
  @objc func didBecomeActiveObserver(notification: NSNotification) {
    viewDataManager.appDidBecomeActiveDate = Date()
    viewDataManager.calculateTimePassed()
  }
}

//MARK: Timer
extension RecipeTableViewCell {
  @IBAction func timerButtonTapped(_ sender: UIButton) {
    viewDataManager.timerButtonTapped()
  }
  
  @IBAction func stopTimerButtonTapped(_ sender: UIButton) {
    viewDataManager.stopTimer()
    updateTimerLabel()
  }
  
  func updateTimerLabel() {
    if timerLabel != nil {
      timerLabel.text = viewDataManager.formatTimerString()
    }
  }
  
}

//MARK: Alert
extension RecipeTableViewCell {
  func presentAlert(title: String, message: String) {
    let ac = UIAlertController(title: title,
                               message: message,
                               preferredStyle: .alert)
    ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    
    UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController?.present(ac, animated: true, completion: nil)
  }
}
