//
//  HeaderViewModel.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 09/07/2021.
//

import Foundation

struct HeaderViewModel {
  let foodImageURL: String
  let title: String
}
