//
//  SelectRecipeTVCellModel.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 09/07/2021.
//

import UIKit

struct SelectRecipeTVCellModel {
  let foodImageURL: String
  let titleTextField: String
  let readyAndServingsLabel: String
  let descriptionTextField: String
}
