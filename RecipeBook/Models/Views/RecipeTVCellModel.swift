//
//  RecipeTVCell.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 09/07/2021.
//

import Foundation

struct RecipeTVCellModel {
  let stepNumber: Int
  let stepDescription: String
  let stepDurationTime: Int
}
