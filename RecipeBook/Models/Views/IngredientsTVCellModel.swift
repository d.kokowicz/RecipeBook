//
//  IngredientsTVCell.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 09/07/2021.
//

import Foundation

struct IngredientsTVCellModel {
  let ingredientName: String
}
