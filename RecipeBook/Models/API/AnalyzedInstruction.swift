//
//  AnalyzedInstruction.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 07/07/2021.
//

import Foundation

private enum CodingKeys: String, CodingKey {
  case steps
}

class AnalyzedInstruction: Decodable {
  var steps: [Step]?

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    steps = try? container.decode([Step].self, forKey: .steps)
  }
}
