//
//  RootRecipeInformation.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 06/07/2021.
//

import Foundation

private enum CodingKeys: String, CodingKey {
  case recipes
}

class RootRecipeInformation: Decodable {
  var recipes: [RecipeInformation]?

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    recipes = try? container.decode([RecipeInformation].self, forKey: .recipes)
  }
}
