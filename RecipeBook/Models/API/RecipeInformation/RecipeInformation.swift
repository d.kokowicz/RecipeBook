//
//  RecipeInformation.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 06/07/2021.
//

import Foundation

private enum CodingKeys: String, CodingKey {
  case id
  case title
  case image
  case servings
  case readyInMinutes
  case instructions
  case extendedIngredients
  case summary
  case dishTypes
  case analyzedInstructions
}

class RecipeInformation: Decodable {
  var id: Int?
  var title: String?
  var image: String?
  var servings: Int?
  var readyInMinutes: Int?
  var instructions: String?
  var extendedIngredients: [ExtendedIngredient]?
  var summary: String?
  var dishTypes: [String]?
  var analyzedInstructions: [AnalyzedInstruction]?

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    id = try? container.decode(Int.self, forKey: .id)
    title = try? container.decode(String.self, forKey: .title)
    image = try? container.decode(String.self, forKey: .image)
    servings = try? container.decode(Int.self, forKey: .servings)
    readyInMinutes = try? container.decode(Int.self, forKey: .readyInMinutes)
    instructions = try? container.decode(String.self, forKey: .instructions)
    extendedIngredients = try? container.decode([ExtendedIngredient].self, forKey: .extendedIngredients)
    summary = try? container.decode(String.self, forKey: .summary)
    dishTypes = try? container.decode([String].self, forKey: .dishTypes)
    analyzedInstructions = try? container.decode([AnalyzedInstruction].self, forKey: .analyzedInstructions)
  }
}
