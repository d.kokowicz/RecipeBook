//
//  ExtendedIngredient.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 06/07/2021.
//

import Foundation

private enum CodingKeys: String, CodingKey {
  case original
}

class ExtendedIngredient: Decodable {
  var name: String?

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    name = try? container.decode(String.self, forKey: .original)
  }
}
