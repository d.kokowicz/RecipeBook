//
//  Step.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 07/07/2021.
//

import Foundation

private enum CodingKeys: String, CodingKey {
  case number
  case step
  case length
}

class Step: Decodable {
  var number: Int?
  var step: String?
  var length: Length?

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    number = try? container.decode(Int.self, forKey: .number)
    step = try? container.decode(String.self, forKey: .step)
    length = try? container.decode(Length.self, forKey: .length)
  }
}
