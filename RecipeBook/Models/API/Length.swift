//
//  Length.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 07/07/2021.
//

import Foundation

private enum CodingKeys: String, CodingKey {
  case number
  case unit
}

class Length: Decodable {
  var number: Int?
  var unit: String?

  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    number = try? container.decode(Int.self, forKey: .number)
    unit = try? container.decode(String.self, forKey: .unit)
  }
}
