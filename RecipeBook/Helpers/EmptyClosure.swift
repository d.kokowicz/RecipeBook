//
//  EmptyClosure.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 25/06/2021.
//

import Foundation

typealias EmptyClosure = (() -> Void)
