//
//  Closure.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 25/06/2021.
//

import Foundation

typealias Closure<ParameterType> = ((ParameterType) -> Void)
