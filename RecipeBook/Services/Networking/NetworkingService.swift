//
//  SpoonacularAPI.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 25/06/2021.
//

import Foundation
import Alamofire
import Promises

struct NetworkingService {
  private var allRecipesURL = "https://api.spoonacular.com/recipes/informationBulk?apiKey=\(Constants.apiKey)&ids=637776,1082047,646974,715540"
    
  func fetchRecipesData() -> Promise<([SelectRecipeTVCellModel], [HeaderViewModel], [[IngredientsTVCellModel]], [[RecipeTVCellModel]])> {
    return Promise { fulfill, reject in
      AF.request(allRecipesURL)
        .validate()
        .responseDecodable(of: [RecipeInformation].self) { (response) in
          
          switch response.result {
          case .success:
            guard let recipeInfo = response.value else {
              return reject(AFError.responseValidationFailed(reason: .dataFileNil))
            }
            
            var selectRecipeTVCellModelArray = [SelectRecipeTVCellModel]()
            for i in 0...3 {
              let selectRecipeTVCellModel = SelectRecipeTVCellModel(
                foodImageURL: recipeInfo[i].image ?? "",
                titleTextField: recipeInfo[i].title ?? "",
                readyAndServingsLabel: "Ready in \(recipeInfo[i].readyInMinutes ?? 0) minutes | \(recipeInfo[i].servings ?? 0) servings",
                descriptionTextField: recipeInfo[i].summary ?? "")
              selectRecipeTVCellModelArray.append(selectRecipeTVCellModel)
            }

            var headerViewModelArray = [HeaderViewModel]()
            for i in 0...3 {
              let headerViewModel = HeaderViewModel(
                foodImageURL: recipeInfo[i].image ?? "",
                title: recipeInfo[i].title ?? "")
              headerViewModelArray.append(headerViewModel)
            }

            var ingredientsCellModelArray = [[IngredientsTVCellModel]]()
            for i in 0...3 {
              let ingredientsCellModel = recipeInfo[i].extendedIngredients?.map { ingredient in IngredientsTVCellModel(ingredientName: ingredient.name ?? "") }
              ingredientsCellModelArray.append(ingredientsCellModel ?? [IngredientsTVCellModel(ingredientName: "")])
            }

            var recipeCellModelArray = [[RecipeTVCellModel]]()
            for i in 0...3 {
              let recipeCellModel = recipeInfo[i].analyzedInstructions?[0].steps?.map { step in RecipeTVCellModel(stepNumber: step.number ?? 0, stepDescription: step.step ?? "", stepDurationTime: step.length?.number ?? 1) }
              recipeCellModelArray.append(recipeCellModel ?? [RecipeTVCellModel(stepNumber: 0, stepDescription: "", stepDurationTime: 0)])
            }
            
            let tuple = (selectRecipeTVCellModelArray, headerViewModelArray, ingredientsCellModelArray, recipeCellModelArray)
            
            NotificationCenter.default.post(name: Constants.didGetData, object: nil, userInfo: ["recipeData": tuple])

            fulfill(tuple)
        
          case .failure(let error):
            reject(error)
          }
        }
    }
    
  }
}
