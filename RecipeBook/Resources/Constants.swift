//
//  Constants.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 25/06/2021.
//

import Foundation

struct Constants {
  static let apiKey = "3b306e09bc294ddaa779e3e6befad0a1"
  static let recipeInformationViewControllerIdentifier = "RecipeInformationViewController"
  static let didGetData = Notification.Name("didGetData")
  static let didConfigureModels = Notification.Name("didConfigureModels")
}
