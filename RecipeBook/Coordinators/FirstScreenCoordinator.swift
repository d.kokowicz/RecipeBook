//
//  FirstScreenCoordinator.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 25/06/2021.
//

import UIKit
import Rswift

final class FirstScreenCoordinator: NSObject {
  var router: FirstScreenRouter
  var networkingService: NetworkingService
  weak var delegate: FirstScreenCoordinatorDelegate?

//services, dependencies
  init(router: FirstScreenRouter, networkingService: NetworkingService, delegate: FirstScreenCoordinatorDelegate?) {
    self.networkingService = networkingService
    self.router = router
    self.delegate = delegate
  }
  
  func start() {
    showFirstScreenModal()
  }
  
  func showSecondScreenModal(headerViewModel: HeaderViewModel?, recipeTVCellModel: [RecipeTVCellModel]?, ingredientsTVCellModel: [IngredientsTVCellModel]?, viewData: RecipeInformationViewData) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let secondScreenViewController = storyboard.instantiateViewController(identifier: Constants.recipeInformationViewControllerIdentifier) as! RecipeInformationViewController
    
    //let secondScreenViewController = R.storyboard.main.secondScreenViewController()!
    
    secondScreenViewController.viewData = viewData
    viewData.headerViewModel = headerViewModel
    viewData.recipeTVCellModel = recipeTVCellModel
    viewData.ingredientsTVCellModel = ingredientsTVCellModel
    secondScreenViewController.coordinator = self
    router.push(secondScreenViewController, animated: true)
  }
  
  func showFirstScreenModal() {
    let firstScreenViewController = R.storyboard.main.firstScreenViewController()!

    firstScreenViewController.coordinator = self
    router.root(firstScreenViewController)
  }
  
}
