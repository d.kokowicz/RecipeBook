//
//  FirstScreenRouter.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 24/06/2021.
//

import UIKit

class AppFlowRouter: BaseRouter {
  private let navigationController: UINavigationController
  
  var topController: UIViewController? {
    return navigationController.viewControllers.last
  }
  
  var rootController: UIViewController {
    return navigationController
  }
  
  // MARK: - Init
  override init() {
    self.navigationController = UINavigationController()
  }
  
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = navigationController.viewControllers.filter({$0.isKind(of: ofClass)}).last {
      navigationController.popToViewController(vc, animated: animated)
    }
  }
  
  // MARK: - Overriding
  override func present(_ viewController: UIViewController,
                        animated: Bool = true,
                        transitionStyle: UIModalTransitionStyle = .crossDissolve,
                        presentationStyle: UIModalPresentationStyle = .fullScreen,
                        completion: EmptyClosure? = nil) {
    viewController.modalTransitionStyle = transitionStyle
    viewController.modalPresentationStyle = .fullScreen
    navigationController.present(viewController, animated: animated)
  }
  
  override func push(_ viewController: UIViewController, animated: Bool = true) {
    navigationController.pushViewController(viewController, animated: animated)
  }
  
  override func pop(animated: Bool = true, completion: EmptyClosure? = nil) {
    navigationController.popViewController(animated: animated)
  }
  
  override func dismiss(animated: Bool = true, completion: EmptyClosure? = nil) {
    navigationController.dismiss(animated: animated, completion: nil)
  }
  
  override func popToRoot(animated: Bool = true, completion: EmptyClosure? = nil) {
    navigationController.popToRootViewController(animated: animated)
  }
  
  override func root(_ viewController: UIViewController, animated: Bool = true, completion: EmptyClosure? = nil) {
    navigationController.setViewControllers([viewController], animated: animated)
  }
}


