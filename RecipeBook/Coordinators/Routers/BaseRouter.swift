//
//  Router.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 24/06/2021.
//

import UIKit

class BaseRouter: NSObject {
  func present(_ viewController: UIViewController,
               animated: Bool = true,
               transitionStyle: UIModalTransitionStyle,
               presentationStyle: UIModalPresentationStyle = .fullScreen,
               completion: EmptyClosure? = nil) { }
  func push(_ viewController: UIViewController, animated: Bool = true) { }
  func pop(animated: Bool = true, completion: EmptyClosure? = nil) { }
  func dismiss(animated: Bool = true, completion: EmptyClosure? = nil) { }
  func popToRoot(animated: Bool = true, completion: EmptyClosure? = nil) { }
  func root(_ viewController: UIViewController, animated: Bool = true, completion: EmptyClosure? = nil) { }
}
