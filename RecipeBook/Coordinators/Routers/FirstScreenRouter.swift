//
//  FirstScreenRouter.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 24/06/2021.
//

import UIKit
import Rswift

class FirstScreenRouter: BaseRouter {
  private let navigationController: UINavigationController
  
  var topController: UIViewController? {
    return navigationController.viewControllers.last
  }
  
  var rootController: UIViewController {
    return navigationController
  }
  
  // MARK: - Init
  override init() {
    self.navigationController = UINavigationController()
    self.navigationController.isNavigationBarHidden = true
  }
  
  // MARK: - Overriding
  override func present(_ viewController: UIViewController,
                        animated: Bool = true,
                        transitionStyle: UIModalTransitionStyle = .crossDissolve,
                        presentationStyle: UIModalPresentationStyle = .fullScreen,
                        completion: EmptyClosure? = nil) {
    viewController.modalTransitionStyle = transitionStyle
    viewController.modalPresentationStyle = .fullScreen
    navigationController.present(viewController, animated: animated)
  }
  
  //go further and show another vc with back button to go back
  override func push(_ viewController: UIViewController, animated: Bool = true) {
    navigationController.pushViewController(viewController, animated: animated)
  }
  
  //Dismisses the view controller that was presented modally by the view controller (without a back button to go back)
  override func dismiss(animated: Bool = true, completion: EmptyClosure? = nil) {
    navigationController.dismiss(animated: animated, completion: nil)
  }
  
  //goes back to the first controller in navigation stack
  override func popToRoot(animated: Bool = true, completion: EmptyClosure? = nil) {
    navigationController.popToRootViewController(animated: animated)
  }
  
  //Pops view controllers until the specified view controller is at the top of the navigation stack.
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = navigationController.viewControllers.filter({$0.isKind(of: ofClass)}).last {
      navigationController.popToViewController(vc, animated: animated)
    }
  }
  
  //sets root vc for nav in this case
  override func root(_ viewController: UIViewController, animated: Bool = true, completion: EmptyClosure? = nil) {
    //Replaces the view controllers currently managed by the navigation controller with the specified items.
    navigationController.setViewControllers([viewController], animated: animated)
  }
}
