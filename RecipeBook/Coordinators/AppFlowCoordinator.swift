//
//  AppFlowCoordinator.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 25/06/2021.
//

import UIKit
import Rswift

final class AppFlowCoordinator: NSObject {
  //reference to all coordinators
  var navigationController: UINavigationController
  var router: FirstScreenRouter?
  var networkingService: NetworkingService!
  private unowned let window: UIWindow
  private var firstScreenCoordinator: FirstScreenCoordinator?
    
  init(window: UIWindow) {
    navigationController = UINavigationController()
    self.window = window
    super.init()
    //all services are created here (not ROUTER) (everything what has service in name)
    networkingService = NetworkingService()
  }
  
  func start() {
    //conditions for running
    startAuthorizationFlow()
  }
  
  private func startAuthorizationFlow() {
    let firstScreenRouter = FirstScreenRouter()
    firstScreenCoordinator = FirstScreenCoordinator(router: firstScreenRouter, networkingService: networkingService, delegate: self)
    
    //appflowcoordinator is a delegate of each another coordinator
    
//    let authorizationRouter = AuthorizationRouter()
//
//    authorizationCoordinator = AuthorizationFlowCoordinator(
//      router: authorizationRouter,
//      delegate: self,
//      networkingService: networkingService,
//      loginService: loginService
//    )
//
    firstScreenCoordinator?.start()
//    authorizationCoordinator?.start()
    window.setRootController(firstScreenRouter.rootController, animated: true)
  }

}

extension AppFlowCoordinator: FirstScreenCoordinatorDelegate {
  func startSecondScreenFlow() {
    //start another coordinator & flow
  }

}

