//
//  RecipeInformationViewData.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 13/07/2021.
//

import Foundation

final class RecipeInformationViewData {
  
  var headerViewModel: HeaderViewModel?
  var recipeTVCellModel: [RecipeTVCellModel]?
  var ingredientsTVCellModel: [IngredientsTVCellModel]?
  
  var sectionsTitles: [String?] = ["Ingredients", "Instructions"]
  
  func numberOfSections() -> Int {
    return sectionsTitles.count
  }
  
  func titleForHeaderInSection(_ section: Int) -> String? {
    return sectionsTitles[section]
  }
  
  func numberOfRowsInSection(_ section: Int) -> Int {
    if section == 0 {
      return ingredientsTVCellModel?.count ?? 0
    }
    if section == 1 {
      return recipeTVCellModel?.count ?? 0
    }
    return sectionsTitles.count
  }
  
}


