//
//  RecipeInformationViewController.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 09/07/2021.
//

import UIKit
import Reusable
import SVProgressHUD

final class RecipeInformationViewController: UIViewController {
  
  weak var coordinator: FirstScreenCoordinator?
  var viewData: RecipeInformationViewData?
  
  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureHeader()
    configureTableView()
    navigationController?.isNavigationBarHidden = false
  }
  
  override func viewWillLayoutSubviews() {
    SVProgressHUD.show()
  }
  
  override func viewDidLayoutSubviews() {
    SVProgressHUD.dismiss()
  }

}

//MARK: Configure Header
extension RecipeInformationViewController: UITableViewDelegate, UITableViewDataSource {
  func configureHeader() {
    let header = HeaderView.loadFromNib()
    header.configure(with: viewData?.headerViewModel)
    tableView.tableHeaderView = header
  }
  
  //MARK: Configure TableView
  func configureTableView() {
    tableView.allowsMultipleSelection = true
    tableView.register(cellType: RecipeTableViewCell.self)
    tableView.register(cellType: IngredientsTableViewCell.self)
    tableView.delegate = self
    tableView.dataSource = self
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return viewData?.numberOfSections() ?? 0
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 30
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    viewData?.numberOfRowsInSection(section) ?? 0
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return viewData?.titleForHeaderInSection(section)
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.section == 0 {
      let cell: IngredientsTableViewCell = tableView.dequeueReusableCell(for: indexPath)
      //let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.orderListTableViewCell, for: indexPath)!
      cell.configure(with: viewData?.ingredientsTVCellModel?[indexPath.row])
      return cell
    } else {
      #warning("Fix R.Swift to change cells & VC")
      let cell: RecipeTableViewCell = tableView.dequeueReusableCell(for: indexPath)
      //let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.orderListTableViewCell, for: indexPath)!
      cell.configure(with: viewData?.recipeTVCellModel?[indexPath.row])
      return cell
    }

  }
}
