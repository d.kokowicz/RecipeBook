//
//  FirstScreenViewController.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 25/06/2021.
//

import UIKit
import SVProgressHUD

final class FirstScreenViewController: UIViewController {

  weak var coordinator: FirstScreenCoordinator?
  
  private var viewData = FirstScreenViewData()
  
  @IBOutlet weak var tableView: UITableView!
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    navigationController?.isNavigationBarHidden = true
  }
  
  override func viewWillLayoutSubviews() {
    SVProgressHUD.show()
  }
  
  override func viewDidLayoutSubviews() {
    SVProgressHUD.dismiss()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    SVProgressHUD.dismiss()
    addObservers()
    viewData.getData()
    configureTableView()
  }
  
  func addObservers() {
    NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name(rawValue: Constants.didConfigureModels.rawValue), object: nil)
  }
}

extension FirstScreenViewController: UITableViewDelegate, UITableViewDataSource {
  func configureTableView() {
    tableView.register(cellType: SelectRecipeTableViewCell.self)
    tableView.delegate = self
    tableView.dataSource = self
  }
  
  @objc func reloadData() {
    DispatchQueue.main.async {
      self.tableView.reloadData()
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return viewData.numberOfSections()
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return viewData.titleForHeaderInSection(section)
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 35
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    UIScreen.main.bounds.width
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell: SelectRecipeTableViewCell = tableView.dequeueReusableCell(for: indexPath)
    //let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.orderListTableViewCell, for: indexPath)!
    cell.configure(with: viewData.selectRecipeCellsModelArray?[indexPath.section])
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    coordinator?.showSecondScreenModal(headerViewModel: viewData.headerViewModelArray?[indexPath.section], recipeTVCellModel: viewData.recipeTVCellModelArray?[indexPath.section], ingredientsTVCellModel: viewData.ingredientsTVCellModelArray?[indexPath.section], viewData: RecipeInformationViewData())
  }
  
}
