//
//  FirstScreenViewData.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 25/06/2021.
//

import Foundation
import NotificationCenter

final class FirstScreenViewData {
  
  init() {
    NotificationCenter.default.addObserver(self, selector: #selector(configureModels(_:)), name: NSNotification.Name(rawValue: Constants.didGetData.rawValue), object: nil)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  let networkingService = NetworkingService()
  
  var sectionsTitles = ["breakfast", "main course", "soup", "salad"]
  
  var selectRecipeCellsModel: SelectRecipeTVCellModel?
  
  var headerViewModelArray: [HeaderViewModel]?
  var recipeTVCellModelArray: [[RecipeTVCellModel]]?
  var selectRecipeCellsModelArray: [SelectRecipeTVCellModel]?
  var ingredientsTVCellModelArray: [[IngredientsTVCellModel]]?
  
  func numberOfSections() -> Int {
    return sectionsTitles.count
  }
  
  func titleForHeaderInSection(_ section: Int) -> String? {
    return sectionsTitles[section]
  }
  
  func getData() {
    networkingService.fetchRecipesData()
  }
  
  @objc func configureModels(_ notification: NSNotification) {
    if let models = notification.userInfo?["recipeData"] as? ([SelectRecipeTVCellModel], [HeaderViewModel], [[IngredientsTVCellModel]], [[RecipeTVCellModel]]) {
      selectRecipeCellsModelArray = models.0
      headerViewModelArray = models.1
      ingredientsTVCellModelArray = models.2
      recipeTVCellModelArray = models.3
      print(#function, #line)
      //notifyVC
      NotificationCenter.default.post(name: Constants.didConfigureModels, object: nil)
    }
  }
  
}
