//
//  AppDelegate.swift
//  RecipeBook
//
//  Created by Dominika Kokowicz on 25/06/2021.
//

import UIKit
import UserNotifications

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  var appFlowCoordinator: AppFlowCoordinator!
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    setupAppCoordinator()
    return true
  }
  
  // MARK: UISceneSession Lifecycle
  private func setupAppCoordinator() {
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.makeKeyAndVisible()
    guard let window = window else { return }
    window.backgroundColor = .white
    appFlowCoordinator = AppFlowCoordinator(window: window)
    appFlowCoordinator.start()
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    print(#function)
  }
    
  func applicationDidBecomeActive(_ application: UIApplication) {
    print(#function)
  }
}

